package com.twuc.webApp.service;

import com.twuc.webApp.dto.ResponseDto;
import com.twuc.webApp.dto.ResponseGuessDto;
import com.twuc.webApp.utils.Constant;
import com.twuc.webApp.utils.RandomNumber;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Stream;

@Service
public class GameService {
    public ResponseGuessDto judgeGuess(Integer answer, Integer result) {
        StringBuilder hint = new StringBuilder();
        if (answer.compareTo(result) == 0) {
            return new ResponseGuessDto("4A0B", true);
        }
        Long correctLength = RandomNumber.getEqualCount(answer, result, 4);
        Long containsLength = RandomNumber.getContainsCount(answer, result, 4);
        hint.append(correctLength).append("A").append(containsLength).append("B");
        return new ResponseGuessDto(hint.toString(), false);
    }

    public ResponseDto getGameStatus(Long gameId) {
        return new ResponseDto(gameId, Constant.gameContainer.get(gameId));
    }

    public void createGame(Long gameId) {
        if (!Constant.gameContainer.containsKey(gameId)) {
            Set<Integer> randomDigital = RandomNumber.generate4RandomNumber();
            Optional<Integer> reduce = randomDigital.stream().reduce((a, b) ->
                    Integer.parseInt(String.valueOf(a).concat(String.valueOf(b))));
            reduce.ifPresent(randomNumber -> {
                Constant.gameContainer.put(gameId, randomNumber);
            });
        }
    }
}
