package com.twuc.webApp.controllerTest;

import com.twuc.webApp.dto.ResponseGuessDto;
import com.twuc.webApp.service.GameService;
import com.twuc.webApp.utils.Constant;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class GameServiceTest {
    private GameService gameService = new GameService();

    @Test
    void should_return_true_response_when_guess_correct_answer() {
        ResponseGuessDto responseGuessDto = gameService.judgeGuess(1234, 1234);
        assertTrue(responseGuessDto.getCorrect());
        assertEquals("4A0B", responseGuessDto.getHint());
    }

    @Test
    void should_return_response_with_hint_when_guess_non_correct_answer() {
        ResponseGuessDto responseGuessDto = gameService.judgeGuess(1324, 4321);
        ResponseGuessDto anotherResponseGuessDto = gameService.judgeGuess(3569, 9875);
        assertFalse(responseGuessDto.getCorrect());
        assertEquals("2A2B", responseGuessDto.getHint());
        assertFalse(anotherResponseGuessDto.getCorrect());
        assertEquals("0A2B", anotherResponseGuessDto.getHint());
    }

    @Test
    void should_create_game_when_call_create_game() {
        gameService.createGame(1L);
        assertTrue(Constant.gameContainer.containsKey(1L));
        assertNotNull(Constant.gameContainer.get(1L));
    }
}
