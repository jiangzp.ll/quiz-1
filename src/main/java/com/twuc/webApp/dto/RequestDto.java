package com.twuc.webApp.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

public class RequestDto {

    @Min(1111) @Max(9999)
    private Integer answer;

    public RequestDto(Integer answer) {
        this.answer = answer;
    }

    public RequestDto() {
    }

    public Integer getAnswer() {
        return answer;
    }
}
