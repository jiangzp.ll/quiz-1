package com.twuc.webApp.utils;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.stream.Stream;

public class RandomNumber {
    public static Set<Integer> generate4RandomNumber() {
        Set<Integer> randomNumberSet = new HashSet<>();
        Random random = new Random();
        while (randomNumberSet.size() < 4) {
            randomNumberSet.add(random.nextInt(9));
        }
        return randomNumberSet;
    }

    public static Long getEqualCount(Integer answer, Integer result, int length) {
        return Stream.iterate(1, i -> i * 10).limit(length).filter(index -> {
            return (answer / index) % 10 == (result / index) % 10;
        }).count();
    }

    public static Long getContainsCount(Integer answer, Integer result, int length) {
        Set<Integer> resultSet = new HashSet<>();
        Stream.iterate(1, i -> i * 10).limit(length).forEach(index -> {
            resultSet.add(answer / index % 10);
            resultSet.add(result / index % 10);
        });
        return 2 * length - resultSet.size() - getEqualCount(answer, result, 4);
    }

}
